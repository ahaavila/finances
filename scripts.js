const Modal = {
  open(){
    // Abrir modal
    // Adicionar a class active ao modal
    document.querySelector('.modal-overlay')
            .classList.add('active')
  },
  close(){
    // Fechar o modal
    // Remover a class active do modal
    document.querySelector('.modal-overlay')
            .classList.remove('active')
  },
  toogle(){
    // Fechar o modal
    // Remover a class active do modal
    document.querySelector('.modal-overlay')
            .classList.toogle('active')
  }
}

const transactions = [
  {
    id: 1,
    description: 'Luz',
    amount: -50001,
    date: '23/01/2021'
  },
  {
    id: 2,
    description: 'Website',
    amount: 500012,
    date: '23/01/2021'
  },
  {
    id: 3,
    description: 'Internet',
    amount: -20000,
    date: '23/01/2021'
  }
]

const Transaction = {
  incomes() {
    // somar as entradas
    let income = 0;

    transactions.forEach((transaction) => {
      if(transaction.amount > 0) {
        income += transaction.amount;
      }
    })

    return income;
  },
  expenses() {
    // somar as saidas
    let expense = 0;

    transactions.forEach((transaction) => {
      if(transaction.amount < 0) {
        expense += transaction.amount;
      }
    })

    return expense;
  },
  total() {
    // entradas - saidas
    return Transaction.incomes() + Transaction.expenses()
  }
}

// Preciso pegar as transactions e substituir os dados 
// da tabela HTML pelas transactions.

const DOM = {
  transactionsContainer: document.querySelector('#data-table tbody'),
  addTransaction(transaction, index) {
    const tr = document.createElement('tr')
    tr.innerHTML = DOM.innerHTMLTransaction(transaction)
    
    DOM.transactionsContainer.appendChild(tr)
  },
  innerHTMLTransaction(transaction) {
    const CSSClass = transaction.amount > 0 ? "income" : "expense"
    const amount = Utils.formatCurrency(transaction.amount)
    const html = `
      <td class="description">${transaction.description}</td>
      <td class="${CSSClass}">${amount}</td>
      <td class="date">${transaction.date}</td>
      <td><img src="./assets/minus.svg" alt="Remover Transação"></td>
    `
    return html
  },
  updateBalance() {
    document.getElementById('incomeDisplay')
            .innerHTML = Utils.formatCurrency(Transaction.incomes())
    document.getElementById('expenseDisplay')
            .innerHTML = Utils.formatCurrency(Transaction.expenses())
    document.getElementById('totalDisplay')
            .innerHTML = Utils.formatCurrency(Transaction.total())
  }
}

const Utils = {
  formatCurrency(value) {
    // Preciso formatar os valores para exibir em tela

    // Pego o sinal separado do valor
    const signal = Number(value) < 0 ? "-" : ""
    // Retiro tudo o que não for numero do valor
    value = String(value).replace(/\D/g, "")
    // Divido esse valor por 100 para deixá-lo decimal
    value = Number(value) / 100
    // Coloco ele com formatação de moeda brasileira
    value = value.toLocaleString("pt-BR", {
      style: "currency",
      currency: "BRL"
    })

    return signal + value
  }
}

// for(i = 0; transactions.length; i++) {
//   DOM.addTransaction(transactions[i])
// }

transactions.forEach(function(transaction) {
  DOM.addTransaction(transaction)
});

DOM.updateBalance();
